# Getting Started

## Projeto

Para instalar as dependências do projeto, executar o comando:

```shell
npm i
```

Para o próximo passo, instalar também a dependência `sequelize-cli` em nível global:

```shell
npm i -g sequelize-cli
```

## Banco de dados

É necessária a utilização de um **banco de dados MySQL**.

Executar o script `mysql_setup` que se encontra na raiz do projeto para a criação do banco de dados que será utilizado, assim como também o usuário que fará as requisições ao banco.

Executado o script, navegar pelo terminal até a raiz do projeto e executar o comando:

```shell
sequelize-cli db:migrate
```

Tendo êxito, executar o seguinte comando para população das tabelas:

```shell
sequelize-cli db:seed:all
```

## Servir o projeto

Para startar o projeto, executar o comando:

```shell
npm run dev
```
