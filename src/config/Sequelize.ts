import {Sequelize as sequelize} from 'sequelize';

const env = process.env.NODE_ENV || 'development';
const config = {
	development: {
		username: "sqool_adm",
		password: "sqoolPass123",
		database: "sqool",
		host: "127.0.0.1",
		dialect: "mysql",
		operatorsAliases: false
	},
	test: {
		username: "sqool_adm",
		password: "sqoolPass123",
		database: "sqool",
		host: "127.0.0.1",
		dialect: "mysql",
		operatorsAliases: false
	},
	production: {
		username: "sqool_adm",
		password: "sqoolPass123",
		database: "sqool_prod",
		host: "127.0.0.1",
		dialect: "mysql",
		operatorsAliases: false
	}
}

export class SequelizeDatabase {

	public static get instance() {
		if (!this._instance) {
			this._instance = new SequelizeDatabase();
		}

		return this._instance;
	};
	public get sequelize() {
		return this._sequelize;
	}
	private static _instance: SequelizeDatabase;
	private _sequelize: any;

	private constructor() {
		this._sequelize = new sequelize(config[env].development, config[env].username, config[env].password, config[env]);
	}
}