import fs from 'fs';
import express, { Router } from 'express';
import color from '../tools/consoleColor';
import RouterResponse from '../libraries/RouterResponse';
import mapRoutes from 'express-routes-mapper';


/**
 * Routes
 *
 * Set dynamic routes
 */
export default class Routes {

    // app
    private __app: express.Express;

    constructor(app: express.Express) {

        // set variables
        this.__app = app;

        // Carrega arquivos de rotas
        this.loadingRoutes();
    }


    /**
     * loadingRoutes
     *
     * set all defaults routes
     *
     * @public
     * @return {void}
     */
    public loadingRoutes(): void {

        // Verifica a autenticação
        // TODO: Cada serviço deve conter, nome do app e chave privada, configurada no firebase
        this.__app.use('*', (_req, _res, next) => next());

        // Carrega as rotas dos serviços e todas as versões
        this.__app.use('/', this.__loadingServices());

        // TODO: Cada serviço deve conter, nome do app e chave privada, configurada no firebase
        this.__app.use('*', (_req, res, _next) => RouterResponse.notFound(res));

        // Impede estourar erro na api
        this.__app.use((err: Error, _req: express.Request, res: express.Response, _next: express.NextFunction) => RouterResponse.serverError(err, res));
    }


    /**
     * __loadingServices
     *
     * load all services and versions
     *
     * @private
     * @return {Router}
     */
    private __loadingServices(): Router {

        // carrega diretório atual e remove a primeira barra
        let path: string = __dirname.replace(process.cwd(), '').substr(1);

        // faz split nos nomes dos diretórios
        const dirs: Array<string> = path.split('/');

        // remove ultimo diretório para voltar uma pasta
        dirs.pop();

        // junta novamente
        path = dirs.join('/');

        // le o nome dos serviços
        const services: any = {};
        fs.readdirSync(`${path}/services`).forEach(dirname => {

            // Pega todas as versões do serviço
            const versions: Array<string> = [];
            fs.readdirSync(`${path}/services/${dirname}`).forEach(version => {
                versions.push(version);
            });

            services[dirname] = versions;

            console.log(`\n    ${color.WARNING}${color.WHITE}${color.BOLD}  LOAD   ${color.END}${color.GREEN}${color.BOLD} ${color.END}  Service ${color.BLUE}${dirname}${color.END} [${ versions.join(', ') }]`);
        });

        // Carrega o arquivo de rota do serviço
        const router: Router = express.Router();
        for(const service of Object.keys(services)) {
            const versions: Array<string> = services[service];

            for(const version of versions) {
                const moduleRouter: any = require(`../services/${service}/${version}/router`);
                const mappedRoutes = mapRoutes(moduleRouter.routes, `${path}/services/${service}/${version}/controllers/`);
                router.use(`/${service}/${version}`, mappedRoutes);
            }
        }

        return router;
    }
}
