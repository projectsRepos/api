const SEQUELIZE_ERRORS = {
	ER_BAD_FIELD_ERROR: 'Unknown column',
};

export default SEQUELIZE_ERRORS;
