export class DocumentValidator {

    /**
     * isCPF
     *
     * Valida CPF
     * Exemplo e regras do site da receita federal
     *
     * @public
     * @param    {string} cpf
     * @return   {Promise}
     */
    public static isCPF(cpf: string): boolean {

        if (!cpf) return false;

        let sum: number = 0;
        let mod: number;

        if(cpf.length > 11) return false;

        // Verifica se todos os digitos não não iguais
        let checkRepeat: number = 0;
        let lastChar: string = '';
        for (const char of cpf) {
            if (char === lastChar || lastChar === '')
                checkRepeat++;
            else
                checkRepeat = 0;

            lastChar = char;
        }
        if (checkRepeat === 11)
            return false;


        // Primeiro digito verificador
        let i = 1;
        while(i <= 9) {
            sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (11 - i);
            i++;
        }

        mod = (sum * 10) % 11;

        if ((mod === 10) || (mod === 11)) mod = 0;
        if (mod !== parseInt(cpf.substring(9, 10), 10)) return false;

        // Segundo digito verificador
        sum = 0;
        i = 1
        while(i <= 10) {
            sum = sum + parseInt(cpf.substring(i - 1, i), 10) * (12 - i);
            i++;
        }

        mod = (sum * 10) % 11;

        if ((mod === 10) || (mod === 11)) mod = 0;
        if (mod !== parseInt(cpf.substring(10, 11), 10)) return false;

        return true;
    }


    /**
     * isCNPJ
     *
     * Valida CNPJ
     * Exemplo e regras do site da receita federal
     *
     * @public
     * @param    {string} cnpj
     * @return   {Promise}
     */
    public static isCNPJ(_cnpj: any):boolean{
        const cnpj = _cnpj.replace(/[^\d]+/g, '');

    	// Valida a quantidade de caracteres
    	if (cnpj.length !== 14) {
    		return false;
        }

    	// Elimina inválidos com todos os caracteres iguais
    	if (/^(\d)\1+$/.test(cnpj)) {
    		return false;
        }

    	// Cáculo de validação
    	const t = cnpj.length - 2,
        d = cnpj.substring(t),
        d1 = parseInt(d.charAt(0)),
        d2 = parseInt(d.charAt(1)),
        calc = x => {
            const n = cnpj.substring(0, x);
            let y = x - 7,
            s = 0,
            r = 0

            for (let i = x; i >= 1; i--) {
                s += n.charAt(x - i) * y--;
                if (y < 2)
                y = 9
            }

            r = 11 - s % 11
            return r > 9 ? 0 : r
        }

    	return calc(t) === d1 && calc(t + 1) === d2;
	}
}
