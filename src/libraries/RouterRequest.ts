import express from 'express';
import { validationResult, checkSchema, Result, ValidationError, ParamSchema } from 'express-validator';
import RouterResponse from './RouterResponse';

export default class RouterRequest {

    /**
     * checkSchema
     *
     * Retornar todos os middlewares necessários para a rota especifica
     *
     * @protected
     * @param schema Record<string, ParamSchema>
     * @return {Array<any>}
     */
    public static checkSchema(schema: Record<string, ParamSchema>): Array<any>  {
        const middlewares: Array<any> = [
            checkSchema(schema),
            RouterRequest.validate
        ]

        return middlewares;
    }


    /**
     * validate
     *
     * método que valida parametros de rota
     *
     * @protected
     * @param {express.Request} req
     * @param {express.Response} res
     * @param {express.NextFunction} next
     * @param {boolean} auth se for validação de autenticação retorna status diferente no request
     * @return {Array<any>}
     */
    public static validate(req: express.Request, res: express.Response, next: express.NextFunction) {
        RouterRequest.__validate(req, res, next);
    }


    /**
     * validate
     *
     * método que valida parametros da autenticação
     *
     * @protected
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     * @return {Array<any>}
     */
    public static validateAuth(req: express.Request, res: express.Response, next: express.NextFunction) {
        RouterRequest.__validate(req, res, next, true);
    }


    /**
     * __validate
     *
     * método que valida middlewares
     *
     * @protected
     * @param {express.Request} req
     * @param {express.Response} res
     * @param {express.NextFunction} next
     * @param {boolean} auth se for validação de autenticação retorna status diferente no request
     * @return {Array<any>}
     */
    private static __validate(req: express.Request, res: express.Response, next: express.NextFunction, auth?: boolean) {
        const errors: Result<ValidationError> = validationResult(req);
        if (!errors.isEmpty()) {
            if(auth) {
                RouterResponse.unauthorizedError(res);
            }
            else {
                RouterResponse.error(errors.array(), res);
            }
        }
        else {
            next();
        }
    }
}
