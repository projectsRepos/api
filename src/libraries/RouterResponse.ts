import express from "express";


/**
 * RouterResponse
 *
 * Set res.json express
 */
export default class RouterResponse {

    /**
     * setCache
     *
     * seta o cache em minutos na requisição
     *
     * @public
     * @param {express.Response} res the response
     * @param {Object} data object to send
     * @return {void}
     */
    public static setCache(minutes: number, res: express.Response): void {
        res.set('Cache-Control', `public, max-age=${minutes * 60}, s-maxage=${minutes * 60}`);
    }


    /**
     * success
     *
     * retorna sucesso e exibe data como resposta - 200
     *
     * @public
     * @param {express.Response} res the response
     * @param {Object} data object to send
     * @return {void}
     */
    public static success(data: Object, res: express.Response): void {
        res.status(200);
        res.json(this.dataResponse(true, data));
    }


    /**
     * error
     *
     * retorno default para erro - 400
     *
     * @public
     * @param {string | Object} error mensagem de retorno ou object
     * @param {express.Response} res the response
     * @return {void}
     */
    public static error(error: string|Object, res: express.Response): void {
        res.status(400);
        res.json(this.dataResponse(false, error));
    }


    /**
     * serverError
     *
     * retorno default para erro - 500
     *
     * @public
     * @param {Error} error data
     * @param {express.Response} res the response
     * @return {void}
     */
    public static serverError(error: Error, res: express.Response): void {
        res.status(500);
        console.error(error); // TODO: Salvar log
        res.json(this.dataResponse(false, error.message));
    }


    /**
     * notFound
     *
     * retorno default para pagina não encontrada - 404
     *
     * @public
     * @param {express.Response} res the response
     * @return {void}
     */
    public static notFound(res: express.Response): void {
        // Seta cache para a rota procurada para 60 minutos
        //res.set('Cache-Control', 'public, max-age=3600, s-maxage=3600'); // TODO: colocar essas configurações de cache no banco futuramente
        res.status(404);
        res.json(this.dataResponse(false, 'Page Not Found'));
    }


    /**
     * unauthorizedError
     *
     * retorno para autenticação não autorizada
     *
     * @public
     * @param {express.Response} res the response
     * @param {string} msg retorno para cliente
     * @return {void}
     */
    public static unauthorizedError(res: express.Response, msg?: string): void {
        res.status(401);
        res.json(this.dataResponse(false, msg ? msg : 'No permission to access this service'));
    }


    /**
     * clearData
     *
     * Limpa todos os campos sem valor do objeto
     *
     * @public
     * @param {any} data
     * @return {any}
     */
    public static clearData(data: any): any {

        // clone object
        const response = Object.assign({}, data);

        // remove chave
        for(const key of Object.keys(response)) {
            if(!response[key]) {
                delete response[key];
            }
        }

        return response;
    }


    /**
     * dataResponse
     *
     * rota default para pagina não encontrada - 404
     *
     * @public
     * @param {boolean} status success = true | failure = false
     * @param {Object} data object to send
     * @return {Object} response data
     */
    public static dataResponse(status: boolean, data: string|Object): Object {
        const response: Object = {
            status: status,
            error: (status) ? false : data,
            date: new Date()
        };

        // Se o status for de sucesso retorna os dados
        if(status)
            response['data'] = data;

        return response;
    }
}
