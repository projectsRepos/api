import express from 'express';
import http from 'http';
import cors from 'cors';
import Router from './config/Router';
import compression from 'compression';
import color from './tools/consoleColor';

// Start firebase admin
const app: express.Express = express();

// Adiciona controladores do express
app.use(express.json()); // Converte o body do request para objeto
app.use(express.urlencoded());
app.use(cors({ origin: true })); // Automaticamente habilita cross-origin requests
app.use(compression()); // Compressão GZIP


// Adiciona as rotas ao express
new Router(app);


// Dev mode start local server
const server: http.Server = http.createServer(app);
const port: number = 4000;

server.listen(port, () => {
    console.log(`\n    ${color.BG_GREEN}${color.WHITE}${color.BOLD}  START  ${color.END}${color.GREEN}${color.BOLD}  ✔ ${color.END}Serve listening on ${color.BLUE}http://127.0.0.1:${port}${color.END}\n`);
});
