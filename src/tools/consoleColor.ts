const color = {
    HEADER    : '\x1b[95m',
    WHITE     : '\x1b[97m',
    BLUE      : '\x1b[34m',
    GREEN     : '\x1b[32m',
    FAIL      : '\x1b[31m',
    END       : '\x1b[0m',
    BOLD      : '\x1b[1m',
    UNDERLINE : '\x1b[4m',
    BG_GREEN  : '\x1b[42m',
    BG_BLUE   : '\x1b[44m',
    WARNING   : '\x1b[43m'
}

export default color;
