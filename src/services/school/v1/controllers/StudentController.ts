import StudentModel from '../models/StudentModel';
import BaseController from './BaseController';

export default class StudentController extends BaseController {

    public static get model() { return StudentModel; }
}
