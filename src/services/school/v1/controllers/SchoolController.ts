import SchoolModel from '../models/SchoolModel';
import BaseController from './BaseController';

export default class SchoolController extends BaseController {

    public static get model() { return SchoolModel; }
}
