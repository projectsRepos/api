import SchoolGroupModel from '../models/SchoolGroupModel';
import BaseController from './BaseController';

export default class SchoolGroupController extends BaseController {

    public static get model() { return SchoolGroupModel; }
}
