import express from 'express';
import { Op } from 'sequelize';
import RouterResponse from '../../../../libraries/RouterResponse';
import SEQUELIZE_ERRORS from '../../../../libraries/utils/SequelizeErrors';
import SchoolModel from '../models/SchoolModel';

export default abstract class BaseController {

    /**
     * Recupera um registro ou uma lista de registros do banco
     * 
     * @public
     * @param req express.Request
     * @param res express.Response
     */
    public async get(req: express.Request, res: express.Response): Promise<void> {
		try {
			if (req.params.id) {
				const find = await req.body.__model.findOne({ where: { id: req.params.id } });
				RouterResponse.success(find, res);
				
	        } else {
				const where: any = {};
				Object.keys(req.query).forEach(key => {
					if (req.query[key]) {
						where[key] = {
							[Op.like]: req.query[key]
	                    };
	                }
	            });
				const find = await req.body.__model.findAll({ where });
				RouterResponse.success(find, res);
	        }
			
            // RouterResponse.setCache(60, res); // 60 minutos de cache
        } catch(error) {
            if (error.name === 'SequelizeDatabaseError') {
                RouterResponse.error(SEQUELIZE_ERRORS[error.parent.code], res);
            } else {
                RouterResponse.error(error, res);
            }
        }
	}
	
	/**
	 * save
	 * 
	 * Salva um registro no banco de dados, criando ou atualizando de acordo com a existência do atributo `id`
	 * 
	 * @param req express.Request
	 * @param res express.Response
     * @return {Response}
	 */
	public async save(req: express.Request, res: express.Response): Promise<void> {
		try {
            const save = await req.body.__model.upsert(req.body, { where: { id: req.body.id } });
            RouterResponse.success(save, res);
        } catch(error) {
            RouterResponse.error(error, res);
        }
	}

    /**
     * delete
     * 
     * Remove um registro
     * 
     * @param req express.Request
     * @param res express.Response
     * @return {Response}
     */
    public async delete(req: express.Request, res: express.Response): Promise<void> {
        try {
            const destroy = await req.body.__model.destroy({ where: { id: req.params.id } });
            RouterResponse.success(destroy, res);
        } catch (error) {
            RouterResponse.error(error, res);
        }
	}
	
	static get model(): any { return null };
}
