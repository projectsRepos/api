import { ParamSchema } from 'express-validator';
import RouterRequest from '../../../../libraries/RouterRequest';
import SchoolModel from '../models/SchoolModel';

export default class SchoolValidator {

    /**
     * Define o Model usado nas requisições desse endpoint
     */
    public static setModel(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            __model: {
                in: 'body',
                custom: {
                    options: (_data, {req}) => {
                        req.body.__model = SchoolModel;
                        return true;
                    }
                },
            },
        };
        return RouterRequest.checkSchema(schema);
    }

    /**
     * postCheckParams
     *
     * Válida todos os campos obrigatórios da requisição
     *
     * @public
     * @return {Array<any>}
     */
    public static saveCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
                in: 'body',
                optional: true,
                isNumeric: true,
                custom: { options: (data, {req}) => {
                    if (data && req.method === 'PUT') {
                        return SchoolModel.findOne({ where: { id: data } }).then(item => {
                            if (!item) {
                                return Promise.reject('School not found');
                            }
                        });
                    }
                    return req.method === 'POST';
                } }
            },
            name: {
                in: 'body',
                isString: true,
            },
            addressStreet: {
                in: 'body',
                isString: true,
            },
            addressNumber: {
                in: 'body',
                isString: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de listagem
     *
     * @public
     * @return {Array<any>}
     */
    public static getCheckParams(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            name: {
                in: 'query',
                optional: true,
                customSanitizer: {
                    options: data => data ? `%${data}%` : '',
                },
            },
            addressStreet: {
                in: 'query',
                optional: true,
                customSanitizer: {
                    options: data => data ? `%${data}%` : '',
                },
            },
            addressNumber: {
                in: 'query',
                optional: true,
                customSanitizer: {
                    options: data => data ? `%${data}%` : '',
                },
            },
            id: {
                in: 'params',
                optional: true,
                isNumeric: true,
            }
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de remoção
     *
     * @public
     * @return {Array<any>}
     */
    public static deleteCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
                in: 'params',
                isNumeric: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }
}
