import { ParamSchema } from 'express-validator';
import RouterRequest from '../../../../libraries/RouterRequest';
import SchoolGroupModel from '../models/SchoolGroupModel';
import SchoolModel from '../models/SchoolModel';

export default class SchoolGroupValidator {

    /**
     * Define o Model usado nas requisições desse endpoint
     */
    public static setModel(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            __model: {
                in: 'body',
                custom: {
                    options: (_data, {req}) => {
                        req.body.__model = SchoolGroupModel;
                        return true;
                    }
                },
            },
        };
        return RouterRequest.checkSchema(schema);
    }

    /**
     * postCheckParams
     *
     * Válida todos os campos obrigatórios da requisição
     *
     * @public
     * @return {Array<any>}
     */
    public static postCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
			schoolId: {
                in: 'body', notEmpty: true,
            },
            name: {
                in: 'body', notEmpty: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de listagem
     *
     * @public
     * @return {Array<any>}
     */
    public static getCheckParams(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            name: {
                in: 'query',
                optional: true,
                customSanitizer: {
                    options: data => data ? `%${data}%` : '',
                },
            },
            schoolId: {
                in: 'query',
                optional: true,
                customSanitizer: {
                    options: data => data ? `${data}` : '',
                },
            },
            id: {
				in: 'params',
				isNumeric: true,
				optional: true
            }
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de atualização
     *
     * @public
     * @return {Array<any>}
     */
    public static saveCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
				in: 'body',
				optional: true,
				isNumeric: true,
				custom: { options: (data, {req}) => {
					if (data && req.methodd === 'PUT') {
						return SchoolGroupModel.findOne({ where: { id: data } }).then(item => {
							if (!item) {
								return Promise.reject('School group not found');
							}
						});
					}
					return req.method === 'POST';
				} }
            },
			schoolId: {
				in: 'body',
				isNumeric: true,
				custom: { options: (data) => {
					if (data) {
						return SchoolModel.findOne({ where: { id: data } }).then(item => {
							if (!item) {
								return Promise.reject('School not found');
							}
						});
					}
					return false;
				} }
            },
            name: {
                in: 'body', notEmpty: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de remoção
     *
     * @public
     * @return {Array<any>}
     */
    public static deleteCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
				in: 'params',
				isNumeric: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }
}
