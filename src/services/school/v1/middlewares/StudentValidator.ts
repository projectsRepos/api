import { ParamSchema } from 'express-validator';
import RouterRequest from '../../../../libraries/RouterRequest';
import StudentModel, { IStudentModel } from '../models/StudentModel';
import SchoolGroupModel, { ISchoolGroupModel } from '../models/SchoolGroupModel';

export default class StudentValidator {

    /**
     * Define o Model usado nas requisições desse endpoint
     */
    public static setModel(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            __model: {
                in: 'body',
                custom: {
                    options: (_data, {req}) => {
                        req.body.__model = StudentModel;
                        return true;
                    }
                },
            },
        };
        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de listagem
     *
     * @public
     * @return {Array<any>}
     */
    public static getCheckParams(): Array<any> {
        const schema: Record<string, ParamSchema> = {
            name: {
                in: 'query',
				optional: true,
				isString: true,
				trim: true,
                customSanitizer: {
                    options: data => data ? `%${data}%` : '',
                },
            },
            schoolGroupId: {
                in: 'query',
				optional: true,
				isNumeric: true,
                customSanitizer: {
                    options: data => data ? `${data}` : '',
                },
            },
            ra: {
                in: 'query',
				optional: true,
				isNumeric: true,
                customSanitizer: {
                    options: data => data ? `${data}` : '',
                },
            },
            id: {
				in: 'params',
				isNumeric: true,
				optional: true
            }
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de criação ou atualização
     *
     * @public
     * @return {Array<any>}
     */
    public static saveCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
				in: 'body',
				isNumeric: true,
				optional: true,
				custom: {
					options: (data, {req}) => {
						if (data && req.method === 'PUT') {
							return StudentModel.findOne({ where: { id: data } }).then(item => {
								if (!item) {
									return Promise.reject('Student not found');
								}
							});
						}
						return req.method === 'POST';
					}
				}
            },
            name: {
				in: 'body',
				isString: true
            },
            schoolGroupId: {
				in: 'body',
				isNumeric: true,
				custom: { options: (data) => {
					if (data) {
						return SchoolGroupModel.findOne({ where: { id: data } }).then(item => {
							if (!item) {
								return Promise.reject('School group not found');
							}
						});
					}
					return false;
				}},
            },
        }

        return RouterRequest.checkSchema(schema);
    }

    /**
     * Valida os campos para requisição de remoção
     *
     * @public
     * @return {Array<any>}
     */
    public static deleteCheckParams(): Array<any> {

        const schema: Record<string, ParamSchema> = {
            id: {
				in: 'params',
				isNumeric: true,
            },
        }

        return RouterRequest.checkSchema(schema);
    }
}
