import { Model, DataTypes } from "sequelize";
import { SequelizeDatabase } from "../../../../config/Sequelize";

export interface ISchoolGroupModel {
	id: number;
	schoolId: number;
	name: string;
	createdAt: Date;
	updatedAt: Date;
}

export default class SchoolGroupModel extends Model implements ISchoolGroupModel {
	public id!: number;
	public schoolId!: number;
	public name!: string;
	public createdAt!: Date;
	public updatedAt!: Date;
}

SchoolGroupModel.init({
	schoolId: {
		type: DataTypes.INTEGER,
		allowNull: false,
		references: {
			model: 'Schools',
			key: 'id'
		},
	},
	name: {
		type: DataTypes.STRING,
		allowNull: false,
	},
}, {
	sequelize: SequelizeDatabase.instance.sequelize,
	tableName: 'School_Groups',
});
