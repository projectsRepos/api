import { Model, DataTypes } from "sequelize";
import { SequelizeDatabase } from "../../../../config/Sequelize";

export interface IStudentModel {
	id: number;
	schoolGroupId: number;
	name: string;
	ra: number;
	createdAt: Date;
	updatedAt: Date;
}

export default class StudentModel extends Model implements IStudentModel {
	public id!: number;
	public schoolGroupId!: number;
	public name!: string;
	public ra: number;
	public birthDate: Date;
	public createdAt!: Date;
	public updatedAt!: Date;
}

StudentModel.init({
	name: {
		type: DataTypes.STRING,
		allowNull: false,
	},
	schoolGroupId: {
		type: DataTypes.INTEGER,
		allowNull: false,
		references: {
			model: 'School_Groups',
			key: 'id'
		},
	},
	ra: DataTypes.INTEGER,
	birthDate: DataTypes.DATE
}, {
	sequelize: SequelizeDatabase.instance.sequelize,
	tableName: 'Students',
});
