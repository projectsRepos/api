import { Model, DataTypes } from "sequelize";
import { SequelizeDatabase } from "../../../../config/Sequelize";

export interface ISchoolModel {
	id: number;
	name: string;
	addressStreet: string;
	addressNumber: number;
	phoneNumber?: number;
	createdAt: Date;
	updatedAt: Date;
}

export default class SchoolModel extends Model implements ISchoolModel {
	public id!: number;
	public name!: string;
	public addressStreet!: string;
	public addressNumber!: number;
	public phoneNumber: number
	public createdAt!: Date;
	public updatedAt!: Date;
}

SchoolModel.init({
	name: DataTypes.STRING,
    addressStreet: DataTypes.STRING,
    addressNumber: DataTypes.INTEGER,
    phoneNumber: DataTypes.INTEGER
}, {
	sequelize: SequelizeDatabase.instance.sequelize,
	tableName: 'Schools',
});