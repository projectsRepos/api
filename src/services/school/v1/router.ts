import SchoolValidator from "./middlewares/SchoolValidator";
import SchoolGroupValidator from './middlewares/SchoolGroupValidator';
import StudentValidator from './middlewares/StudentValidator';

export const routes = {
    /**
     * Schools
     */
    'GET /school': {
        path: 'SchoolController.get',
        middlewares: [SchoolValidator.setModel(), SchoolValidator.getCheckParams()]
    },
    'GET /school/:id': {
        path: 'SchoolController.get',
        middlewares: [SchoolValidator.setModel(), SchoolValidator.getCheckParams()]
    },
    'POST /school': {
        path: 'SchoolController.save',
        middlewares: [SchoolValidator.setModel(), SchoolValidator.saveCheckParams()]
    },
    'PUT /school': {
        path: 'SchoolController.save',
        middlewares: [SchoolValidator.setModel(), SchoolValidator.saveCheckParams()]
    },
    'DELETE /school/:id': {
        path: 'SchoolController.delete',
        middlewares: [SchoolValidator.setModel(), SchoolValidator.deleteCheckParams()]
    },

    /**
     * School Groups
     */
    'GET /school_group': {
        path: 'SchoolGroupController.get',
        middlewares: [SchoolGroupValidator.setModel(), SchoolGroupValidator.getCheckParams()]
    },
    'GET /school_group/:id': {
        path: 'SchoolGroupController.get',
        middlewares: [SchoolGroupValidator.setModel(), SchoolGroupValidator.getCheckParams()]
    },
    'POST /school_group': {
        path: 'SchoolGroupController.save',
        middlewares: [SchoolGroupValidator.setModel(), SchoolGroupValidator.saveCheckParams()]
    },
    'PUT /school_group': {
        path: 'SchoolGroupController.save',
        middlewares: [SchoolGroupValidator.setModel(), SchoolGroupValidator.saveCheckParams()]
    },
    'DELETE /school_group/:id': {
        path: 'SchoolGroupController.delete',
        middlewares: [SchoolGroupValidator.setModel(), SchoolGroupValidator.deleteCheckParams()]
    },

     /**
     * Students
     */
    'GET /student': {
        path: 'SchoolGroupController.get',
        middlewares: [StudentValidator.setModel(), StudentValidator.getCheckParams()]
    },
    'GET /student/:id': {
        path: 'SchoolGroupController.get',
        middlewares: [StudentValidator.setModel(), StudentValidator.getCheckParams()]
    },
    'POST /student': {
        path: 'SchoolGroupController.save',
        middlewares: [StudentValidator.setModel(), StudentValidator.saveCheckParams()]
    },
    'PUT /student': {
        path: 'SchoolGroupController.save',
        middlewares: [StudentValidator.setModel(), StudentValidator.saveCheckParams()]
    },
    'DELETE /student/:id': {
        path: 'SchoolGroupController.delete',
        middlewares: [StudentValidator.setModel(), StudentValidator.deleteCheckParams()]
    }
};
