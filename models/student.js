'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    schoolGroupId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'School_Groups',
        key: 'id'
      },
    },
    ra: DataTypes.INTEGER,
    birthDate: DataTypes.DATE
  }, {});
  Student.associate = function(models) {
    // associations can be defined here
  };
  return Student;
};