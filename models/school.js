'use strict';
module.exports = (sequelize, DataTypes) => {
  const School = sequelize.define('School', {
    name: DataTypes.STRING,
    addressStreet: DataTypes.STRING,
    addressNumber: DataTypes.INTEGER,
    phoneNumber: DataTypes.INTEGER
  }, {});
  School.associate = function(models) {
    // associations can be defined here
  };
  return School;
};