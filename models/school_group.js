'use strict';
module.exports = (sequelize, DataTypes) => {
  const School_Group = sequelize.define('School_Group', {
    schoolId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Schools',
        key: 'id'
      },
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {});
  School_Group.associate = function(models) {
    // associations can be defined here
  };
  return School_Group;
};