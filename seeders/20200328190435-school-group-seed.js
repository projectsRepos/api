'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('School_Groups', [
      { name: '5º A', schoolId: 1, createdAt: new Date(), updatedAt: new Date() },
      { name: '5º B', schoolId: 1, createdAt: new Date(), updatedAt: new Date() },
      { name: '7º C', schoolId: 2, createdAt: new Date(), updatedAt: new Date() },
      { name: '8º D', schoolId: 2, createdAt: new Date(), updatedAt: new Date() },
      { name: '8º A', schoolId: 1, createdAt: new Date(), updatedAt: new Date() },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
   return queryInterface.bulkDelete('School_Groups', null, {});
  }
};
