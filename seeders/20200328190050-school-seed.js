'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Schools', [{
        name: 'Escola Costa Rica',
        addressStreet: 'Rua da Conceição',
        addressNumber: 330,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Escola Barra Funda',
        addressStreet: 'Rua Frederico Cardoso',
        addressNumber: 551,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Schools', null, {});
  }
};
