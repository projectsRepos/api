'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert('Students', [
      { name: 'Anderson Camargo', schoolGroupId: 1, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'Britney Silva', schoolGroupId: 1, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'David Jhonatan', schoolGroupId: 2, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'Letícia Clara', schoolGroupId: 3, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'Derik David', schoolGroupId: 1, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'Franklyn Garcia', schoolGroupId: 5, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
      { name: 'Diego Frodo', schoolGroupId: 3, ra: Math.floor(Math.random()*(10-0+1)+0), createdAt: new Date(), updatedAt: new Date() },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Students', null, {});
  }
};
